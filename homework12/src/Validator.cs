﻿using homework12.src.Interfaces;

namespace homework12.src
{
    class Validator : IValidator
    {
        private IGameResult _gameResult;

        public Validator(IGameResult gameResult)
        {
            _gameResult = gameResult;
        }

        public ITryResult Validate(int expectedValue, int tryValue)
        {
            if (tryValue == expectedValue)
            {
                return _gameResult.GetEqualResult();
            }

            if (tryValue > expectedValue)
            {
                return _gameResult.GetLargerResult();
            }

            return _gameResult.GetSmallerResult();
        }
    }
}
