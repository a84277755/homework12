﻿using homework12.src.Interfaces;
using System;

namespace homework12.src
{
    class NumberGenerator : INumberGenerator
    {
        public int Generate(int minValue, int maxValue)
        {
            Random rand = new Random();
            return rand.Next(minValue, maxValue);
        }
    }
}
