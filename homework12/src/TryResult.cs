﻿using homework12.src.Interfaces;

namespace homework12.src
{
    class TryResult : ITryResult
    {
        public bool IsCorrect { get; set; }
        public bool IsLargerThenExpected { get; set; }
        public bool IsSmallerThenExpected { get; set; }
    }
}
