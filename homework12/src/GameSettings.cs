﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework12.src
{
    class GameSettings
    {
        private int maxValue;
        private int minValue;
        private int numberOfAttempts;

        public GameSettings(int maxValue = 10, int minValue = 1, int numberOfAttempts = 2)
        {
            this.maxValue = maxValue;
            this.minValue = minValue;
            this.numberOfAttempts = numberOfAttempts;
        }

        public int MaxValue => maxValue;
        public int MinValue => minValue;
        public int NumberOfAttempts => numberOfAttempts;
    }
}
