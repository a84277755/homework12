﻿using homework12.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace homework12.src
{
    class ConsoleDataWriter : IDataWriter
    {
        public virtual void WriteMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
