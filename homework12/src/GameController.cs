﻿using homework12.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace homework12.src
{
    class GameController : IGameController
    {
        private GameSettings _gameSettings;
        private IValidator _validator;
        private IDataReader _dataReader;
        private IGameEventsMessages _gameEventsMessages;
        private INumberGenerator _numberGenerator;

        private int triesLeft;
        private int hiddenValue;

        public GameController(GameSettings gameSettings, IValidator validator, IDataReader dataReader,
            INumberGenerator numberGenerator, IGameEventsMessages gameEventsMessages)
        {
            _gameSettings = gameSettings;
            _validator = validator;
            _dataReader = dataReader;
            _numberGenerator = numberGenerator;
            _gameEventsMessages = gameEventsMessages;
        }

        public void StartGame()
        {
            triesLeft = _gameSettings.NumberOfAttempts;
            hiddenValue = _numberGenerator.Generate(_gameSettings.MinValue, _gameSettings.MaxValue);
            _gameEventsMessages.ShowInstructions(_gameSettings.MinValue, _gameSettings.MaxValue, _gameSettings.NumberOfAttempts);

            PlayGame();
        }

        private void PlayGame()
        {
            while (triesLeft > 0)
            {
                try
                {
                    int inputtedValue = _dataReader.readNumber();
                    ITryResult result = _validator.Validate(hiddenValue, inputtedValue);
                    _gameEventsMessages.HandleResult(result, triesLeft - 1);
                    
                    if (result.IsCorrect) break;
                    triesLeft--;
                }
                catch (Exception e)
                {
                    _gameEventsMessages.ShowExceptionMessage(e);
                }
            }

            if (triesLeft == 0)
            {
                _gameEventsMessages.ShowLooseMessage(hiddenValue);
            }

            StartGame();
        }
    }
}
