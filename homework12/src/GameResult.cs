﻿using homework12.src.Interfaces;

namespace homework12.src
{
    class GameResult : IGameResult
    {

        public ITryResult GetEqualResult()
        {
            return new TryResult() { IsCorrect = true };
        }

        public ITryResult GetLargerResult()
        {
            return new TryResult() { IsLargerThenExpected = true };
        }

        public ITryResult GetSmallerResult()
        {
            return new TryResult() { IsSmallerThenExpected = true };
        }
    }
}
