﻿using System;
using System.Collections.Generic;
using System.Text;
using homework12.src.Interfaces;

namespace homework12.src
{
    class ConsoleDataReader : IDataReader
    {
        public int readNumber()
        {
            string inputtedString = Console.ReadLine();
            int potentialNumber;

            bool isCorrect = int.TryParse(inputtedString, out potentialNumber);

            if (!isCorrect)
            {
                throw new ArgumentException("Invalid input value (" + inputtedString + ")");
            } else
            {
                return potentialNumber;
            }
        }
    }
}
