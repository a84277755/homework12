﻿using homework12.src.Interfaces;
using System;

namespace homework12.src
{
    class GameEventsMessages : ConsoleDataWriter, IGameEventsMessages
    {
        public override void WriteMessage(string message)
        {
            Console.WriteLine("> " + message);
        }

        public void ShowExceptionMessage(Exception exception)
        {
            WriteMessage(exception.Message);
        }

        public void ShowExceptionMessage(ArgumentException exception)
        {
            WriteMessage(exception.Message);
        }

        public void ShowLooseMessage(int hiddenValue)
        {
            WriteMessage("=============================");
            WriteMessage("You lost. Hidden value was: " + hiddenValue);
            WriteMessage("=============================");
        }

        public void ShowInstructions(int minValue, int maxValue, int numberOfAttempts)
        {
            WriteMessage("Wellcome to our game");
            WriteMessage("To win you need to guess the hidden value. Otherwise you'll loose.");
            WriteMessage("Min value: " + maxValue + " , Max value: " + minValue + " Attempts: " + numberOfAttempts);
        }

        public void HandleResult(ITryResult result, int triesLeft)
        {
            if (result.IsCorrect)
            {
                WriteMessage("=============================");
                WriteMessage("You win! Attempts left: " + triesLeft);
                WriteMessage("=============================");
            }

            if (result.IsLargerThenExpected)
            {
                WriteMessage("Your value is larger then hidden. Attempts left: " + triesLeft);
            }

            if (result.IsSmallerThenExpected)
            {
                WriteMessage("Your value is smaller then hidden. Attempts left: " + triesLeft);
            }
        }
    }
}
