﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework12.src.Interfaces
{
    interface INumberGenerator
    {
        public int Generate(int minValue, int maxValue);
    }
}
