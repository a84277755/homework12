﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework12.src.Interfaces
{
    interface IDataWriter
    {
        void WriteMessage(string message);
    }
}
