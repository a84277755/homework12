﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework12.src.Interfaces
{
    interface IDataReader
    {
        int readNumber();
    }
}
