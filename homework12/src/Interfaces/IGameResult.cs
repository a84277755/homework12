﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework12.src.Interfaces
{
    interface IGameResult
    {
        public ITryResult GetEqualResult();
        public ITryResult GetLargerResult();
        public ITryResult GetSmallerResult();
    }
}
