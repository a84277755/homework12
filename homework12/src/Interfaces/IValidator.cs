﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework12.src.Interfaces
{
    interface IValidator
    {
        public ITryResult Validate(int expectedValue, int tryValue);
    }
}
