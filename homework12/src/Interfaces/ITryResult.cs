﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework12.src.Interfaces
{
    interface ITryResult
    {
        public bool IsCorrect { get; set; }
        public bool IsLargerThenExpected { get; set; }
        public bool IsSmallerThenExpected { get; set; }
    }
}
