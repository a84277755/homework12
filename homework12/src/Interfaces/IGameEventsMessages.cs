﻿using System;
using System.Collections.Generic;
using System.Text;

namespace homework12.src.Interfaces
{
    interface IGameEventsMessages
    {
        void ShowExceptionMessage(Exception exception);

        void ShowLooseMessage(int hiddenValue);

        void ShowInstructions(int minValue, int maxValue, int numberOfAttempts);

        void HandleResult(ITryResult result, int triesLeft);
    }
}
