﻿using homework12.src;

namespace homework12
{
    class Program
    {
        static void Main(string[] args)
        {
            GameSettings gameSettings = new GameSettings(10, 1, 2);
            ConsoleDataReader dataReader = new ConsoleDataReader();

            GameResult gameResult = new GameResult();
            Validator validator = new Validator(gameResult);
            NumberGenerator numberGenerator = new NumberGenerator();
            GameEventsMessages gameEventsMessages = new GameEventsMessages();

            GameController gameController = new GameController(gameSettings, validator, dataReader, numberGenerator, gameEventsMessages);
            
            gameController.StartGame();
        }
    }
}
